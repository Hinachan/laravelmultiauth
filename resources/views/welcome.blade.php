@extends('layouts.layout')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <h1>利用者(users)</h1>
            <table class="table table-sm">
                <thead>
                    <tr>
                        <th>ログイン用メール</th>
                        <th>パスワード</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                    <tr>
                        <td>{{ $user->email }}</td>
                        <td>password</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @guest('web')
            <form action="{{ route('login') }}" method="post">
                <div class="form-group">
                    <label for="email-user">メールアドレス</label>
                    <input type="email" name="email" class="form-control" id="email-user" aria-describedby="emailHelp" placeholder="メールアドレス">
                </div>
                <div class="form-group">
                    <label for="password-user">パスワード</label>
                    <input type="password" name="password" class="form-control" id="password-user" placeholder="パスワード">
                </div>
                <button type="submit" class="btn btn-primary">利用者ログイン</button>
                @csrf
            </form>
            @endguest
            @auth('web')
            <form action="{{ route('logout') }}" method="post">
                <button type="submit" class="btn btn-primary">ログアウト</button>
                @csrf
            </form>
            @endauth
            <a href="{{ route('user.index') }}">ユーザページへ</a>
        </div>
        <div class="col-lg-4">
            <h1>店舗(stores)</h1>
            <table class="table table-sm">
                <thead>
                    <tr>
                        <th>ログイン用メール</th>
                        <th>パスワード</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($stores as $store)
                    <tr>
                        <td>{{ $store->email }}</td>
                        <td>password</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @guest('store')
            <form action="{{ route('store.login') }}" method="post">
                <div class="form-group">
                    <label for="email-store">メールアドレス</label>
                    <input type="email" name="email" class="form-control" id="email-store" aria-describedby="emailHelp" placeholder="メールアドレス">
                </div>
                <div class="form-group">
                    <label for="password-store">パスワード</label>
                    <input type="password" name="password" class="form-control" id="password-store" placeholder="パスワード">
                </div>
                <button type="submit" class="btn btn-warning">店舗ログイン</button>
                @csrf
            </form>
            @endguest
            @auth('store')
            <form action="{{ route('store.logout') }}" method="post">
                <button type="submit" class="btn btn-warning">ログアウト</button>
                @csrf
            </form>
            @endauth
            <a href="{{ route('store.index') }}">店舗ページへ</a>
        </div>
        <div class="col-lg-4">
            <h1>管理者(managers)</h1>
            <table class="table table-sm">
                <thead>
                    <tr>
                        <th>ログイン用メール</th>
                        <th>パスワード</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($managers as $manager)
                    <tr>
                        <td>{{ $manager->email }}</td>
                        <td>password</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @guest('manager')
            <form action="{{ route('manager.login') }}" method="post">
                <div class="form-group">
                    <label for="email-manager">メールアドレス</label>
                    <input type="email" name="email" class="form-control" id="email-manager" aria-describedby="emailHelp" placeholder="メールアドレス">
                </div>
                <div class="form-group">
                    <label for="password-manager">パスワード</label>
                    <input type="password" name="password" class="form-control" id="password-manager" placeholder="パスワード">
                </div>
                <button type="submit" class="btn btn-success">管理者ログイン</button>
                @csrf
            </form>
            @endguest
            @auth('manager')
            <form action="{{ route('manager.logout') }}" method="post">
                <button type="submit" class="btn btn-success">ログアウト</button>
                @csrf
            </form>
            @endauth
            <a href="{{ route('manager.index') }}">管理者ページへ</a>
        </div>
    </div>
</div>
@endsection
