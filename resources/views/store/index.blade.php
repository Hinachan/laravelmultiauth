@extends('layouts.layout')

@section('content')
<h1>Store</h1>
<p>ログインに使用したメールアドレス：{{ Auth::user()->email }}</p>
<form action="{{ route('store.logout') }}" method="post">
    <button type="submit" class="btn btn-warning">ログアウト</button>
    @csrf
</form>
@endsection
