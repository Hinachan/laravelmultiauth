@extends('layouts.layout')

@section('content')
<h1>Manager</h1>
<p>ログインに使用したメールアドレス：{{ Auth::user()->email }}</p>
<form action="{{ route('manager.logout') }}" method="post">
    <button type="submit" class="btn btn-success">ログアウト</button>
    @csrf
</form>
@endsection
