@extends('layouts.layout')

@section('content')
<h1>User</h1>
<p>ログインに使用したメールアドレス：{{ Auth::user()->email }}</p>
    <form action="{{ route('logout') }}" method="post">
        <button type="submit" class="btn btn-primary">ログアウト</button>
        @csrf
    </form>
@endsection
