<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $users = App\User::all();
    $managers = App\Manager::all();
    $stores = App\Store::all();
    return view('welcome', compact('users', 'stores', 'managers'));
})->name('welcome');

Route::post('login', 'Auth\LoginController@login')->name('login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('user/index', function(){
    return view('user.index');
})->name('user.index')->middleware('auth:web');

Route::post('store/login', 'Store\LoginController@login')->name('store.login');
Route::post('store/logout', 'Store\LoginController@logout')->name('store.logout');
Route::get('store/index', function(){
    return view('store.index');
})->name('store.index')->middleware('auth:store');

Route::post('manager/login', 'Manager\LoginController@login')->name('manager.login');
Route::post('manager/logout', 'Manager\LoginController@logout')->name('manager.logout');
Route::get('manager/index', function(){
    return view('manager.index');
})->name('manager.index')->middleware('auth:manager');
