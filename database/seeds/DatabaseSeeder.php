<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(StoresTableSeeder::class);
        $this->call(ManagersTableSeeder::class);
    }
}

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->delete();
        factory(App\User::class, 10)->create();
    }
}

class StoresTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('stores')->delete();
        factory(App\Store::class, 10)->create();
    }
}

class ManagersTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('managers')->delete();
        factory(App\Manager::class, 10)->create();
    }
}
