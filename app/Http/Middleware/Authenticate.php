<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{

    /** ガード */
    protected $guards = [];

    /**
     *
     */
    public function handle($request, Closure $next, ...$guards)
    {
        $this->guards = $guards;
        return parent::handle($request, $next, ...$guards);
    }

    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (!$request->expectsJson()) {
            if (in_array('store', $this->guards, true)) {
                // storeでログインしていない場合の戻り先
                return route('welcome');
            } else if (in_array('manager', $this->guards, true)) {
                // managerでログインしていない場合の戻り先
                return route('welcome');
            }
            // webでログインしていない場合の戻り先
            return route('welcome');
        }
    }
}
