<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:manager')->except('logout');
    }

    /**
     * ログイン後のリダイレクト先を返します。
     */
    public function redirectTo()
    {
        return redirect()->route('manager.index');
    }

    /**
     * 利用するガードを返します。
     */
    public function guard()
    {
        return Auth::guard('manager');
    }

    /**
     * ログアウトを実行します。
     */
    public function logout()
    {
        $this->guard()->logout();
        return redirect()->route('welcome');
    }
}
