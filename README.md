
# LaravelMultiAuth(Laravelを利用した認証)

Laravelのマルチ認証を実行するまでを実装していあるモジュールです。

## 以下のガードが用意されています。

- web(利用者(default))
- store(店舗)
- manager(管理者)

## 使い方
1.クローン作成後、クローンしたディレクトに移動してLaravelをインストールします。
`composer install`

2.設定ファイルをコピーします。
`cd .env.example .env`

3.APP_KEYを作成します。
`php artisan key:generate`

4.データベースへの接続設定をしてください。
>DB_CONNECTION=mysql
>DB_HOST=127.0.0.1
>DB_PORT=3306
>DB_DATABASE=laravel
>DB_USERNAME=root
>DB_PASSWORD=

5.マイグレーションを実行します。
`php artisan migrate --seed`
